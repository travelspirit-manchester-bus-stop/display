from django.db import models

class New(models.Model):
    image = models.ImageField(
        upload_to='upload/news/',  # File will be uploaded to MEDIA_ROOT/expenses
    )
    phone_number = models.CharField(max_length=30)

    # Control
    added = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.image
