from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import render

from .forms import *

def show(request):
    try:
        new = New.objects.all()[0]
        context = {
            "new": new,
        }
        return render(request, 'news/show.html', context)
    except IndexError:  # When the database doens't have a image.
        return HttpResponseRedirect(
            reverse('submit')
        )

def submit(request):
    formset = NewForm(
        request.POST or None,
        request.FILES or None,
    )

    if formset.is_valid():
        new = formset.save()
        return HttpResponseRedirect(
            reverse('show')
        )
    
    context = {
        "formset": formset,
    }
    return render(request, 'news/form.html', context)

