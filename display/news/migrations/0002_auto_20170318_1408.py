# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-03-18 14:08
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='new',
            name='image',
            field=models.ImageField(upload_to='upload/news/'),
        ),
    ]
