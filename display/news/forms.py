from django.forms import ModelForm

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, ButtonHolder, Submit, HTML

from .models import *

class NewForm(ModelForm):
    class Meta:
        model = New
        fields = [
            'image','phone_number'
        ]

    required_css_class = 'form-field-required'

    def __init__(self, *args, **kwargs):
        super(NewForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper()
        self.helper.layout = Layout(
            Fieldset(
                '',
                'image',
                ButtonHolder(
                    Submit('submit', 'Update')
                )
                )
            )
