Setup Instructions
==================

Dependencies
------------

::

   $ python -m pip install -r requirements.txt

First time run
--------------

::

   $ cd display
   $ python manage.py migrate
   $ python manage.py runserver
